from typing import Dict, List, Any
from datamodel import OrderDepth, TradingState, Order, ProsperityEncoder, Symbol
import json

PEARL = 20
COCONUT = 600
PINA_COLADA = 300
class Logger:
    def __init__(self) -> None:
        self.logs = ""

    def print(self, *objects: Any, sep: str = " ", end: str = "\n") -> None:
        self.logs += sep.join(map(str, objects)) + end

    def flush(self, state: TradingState, orders: dict[Symbol, list[Order]]) -> None:
        print(json.dumps({
            "state": state,
            "orders": orders,
            "logs": self.logs,
        }, cls=ProsperityEncoder, separators=(",", ":"), sort_keys=True))

        self.logs = ""

logger = Logger()

class Trader:

    def run(self, state: TradingState) -> Dict[str, List[Order]]:
        """
        Only method required. It takes all buy and sell orders for all symbols as an input,
        and outputs a list of orders to be sent
        """
        # Initialize the method output dict as an empty dict
        result = {}

        # Iterate over all the keys (the available products) contained in the order depths
        for product in state.order_depths.keys():
            if product == "BERRIES":
                order_depth: OrderDepth = state.order_depths[product]
                # Initialize the list of Orders to be sent as an empty list
                orders: list[Order] = []
                cur_position = state.position.get(product, 0)
                if state.timestamp in range(20000, 300000):
                    if len(order_depth.sell_orders) > 0:
                        ask_options = sorted(list(order_depth.sell_orders.keys()))
                        best_ask = min(order_depth.sell_orders.keys())
                        best_ask_volume = order_depth.sell_orders[best_ask]

                        second_ask = ask_options[1]
                        second_ask_volume = order_depth.sell_orders[second_ask]
                        if cur_position - best_ask_volume <= 250:
                            logger.print("BUY MAYBERRIES: ", str(-best_ask_volume) + "x", best_ask)
                            orders.append(Order(product, best_ask, -best_ask_volume))

                        if cur_position - best_ask_volume - second_ask_volume <= 250:
                            logger.print("BUY MAYBERRIES: ", str(-second_ask_volume) + "x", second_ask)
                            orders.append(Order(product, second_ask, -second_ask_volume))

                elif state.timestamp in range(450000, 600000):
                    if len(order_depth.buy_orders) != 0:
                        bid_options = sorted(list(order_depth.buy_orders.keys()), reverse=True)
                        best_bid = max(order_depth.buy_orders.keys())
                        best_bid_volume = order_depth.buy_orders[best_bid]

                        second_bid = bid_options[1]
                        second_bid_volume = order_depth.buy_orders[second_bid]

                    
                        if cur_position - 10 - best_bid_volume >= -250:
                            logger.print("SELL MAYBERRIES: ", str(best_bid_volume) + "x", best_bid)
                            orders.append(Order(product, best_bid, -best_bid_volume - 10))

                        if cur_position - 20 - best_bid_volume - second_bid_volume >= -250:
                            logger.print("SELL MAYBERRIES: ", str(second_bid_volume) + "x", second_bid)
                            orders.append(Order(product, second_bid, -second_bid_volume - 10))
                result[product] = orders

            if product == "COCONUTS":
                order_depth: OrderDepth = state.order_depths[product]
                # Initialize the list of Orders to be sent as an empty list
                orders: list[Order] = []

                acceptable_price = 8000
                cur_position = state.position.get(product, 0)

                if len(order_depth.sell_orders) > 0 or len(order_depth.buy_orders) != 0:
                    if len(order_depth.buy_orders) != 0:
                        best_bid = max(order_depth.buy_orders.keys())
                        best_bid_volume = order_depth.buy_orders[best_bid]
                    
                    if len(order_depth.sell_orders) > 0:
                        # Sort all the available sell orders by their price,
                        # and select only the sell order with the lowest price
                        best_ask = min(order_depth.sell_orders.keys())
                        best_ask_volume = order_depth.sell_orders[best_ask]

                    if cur_position - best_bid_volume > 0 and best_bid > acceptable_price:
                        logger.print("SELL COCONUT: ", str(best_bid_volume) + "x", best_bid)
                        orders.append(Order(product, best_bid, -best_bid_volume))

                
                    # Check if the lowest ask (sell order) is lower than the above defined fair value
                    if cur_position - best_ask_volume <= 100 and best_ask < acceptable_price:
                        logger.print("BUY COCONUT: ", str(-best_ask_volume) + "x", best_ask, 'pos:', cur_position)
                        orders.append(Order(product, best_ask, best_ask_volume//2))

                result[product] = orders

            if product == "PINA_COLADAS":
                order_depth: OrderDepth = state.order_depths[product]
                # Initialize the list of Orders to be sent as an empty list
                orders: list[Order] = []

                acceptable_price = 15000
                cur_position = state.position.get(product, 0)


                if len(order_depth.buy_orders) != 0:
                    best_bid = max(order_depth.buy_orders.keys())
                    best_bid_volume = order_depth.buy_orders[best_bid]
                    
                    if best_bid > acceptable_price and cur_position - best_bid_volume >= PINA_COLADA:
                        logger.print("SELL PINA: ", str(best_bid_volume) + "x", best_bid, 'pos:', cur_position, 'posCoconut:', state.position.get("COCONUTS", 0))
                        orders.append(Order(product, best_bid, -best_bid_volume))
                

                result[product] = orders

            # if product == "BANANAS":
            #     order_depth: OrderDepth = state.order_depths[product]
            #     orders: list[Order] = []
            #     acceptable_price = 4870
            #     cur_position = state.position.get(product, 0)

            #     if len(order_depth.buy_orders) != 0:
            #         best_bid = max(order_depth.buy_orders.keys())
            #         best_bid_volume = order_depth.buy_orders[best_bid]
                    
            #         if best_bid >= 4880:
            #             logger.print("SELL BANANA: ", str(best_bid_volume) + "x", best_bid, 'pos:', cur_position)
            #             orders.append(Order(product, best_bid, -best_bid_volume))

            #     if len(order_depth.sell_orders) != 0:
            #         best_ask = min(order_depth.sell_orders.keys())
            #         best_ask_volume = order_depth.sell_orders[best_ask]
            #         if best_ask <= 4870 and cur_position + best_ask_volume:
            #             logger.print("BUY : ", str(best_ask_volume) + "x", best_ask, 'pos:', cur_position)
            #             orders.append(Order(product, best_ask, -best_ask_volume))
            #     result[product] = orders

            # Check if the current product is the 'PEARLS' product, only then run the order logic
            if product == 'PEARLS':

                # Retrieve the Order Depth containing all the market BUY and SELL orders for PEARLS
                order_depth: OrderDepth = state.order_depths[product]
                # Initialize the list of Orders to be sent as an empty list
                orders: list[Order] = []

                # Define a fair value for the PEARLS.
                # Note that this value of 1 is just a dummy value, you should likely change it!
                acceptable_price = 10000
                cur_position = state.position.get(product, 0)
                #print(cur_position)

                # If statement checks if there are any SELL orders in the PEARLS market
                if len(order_depth.sell_orders) > 0:

                    # Sort all the available sell orders by their price,
                    # and select only the sell order with the lowest price
                    best_ask = min(order_depth.sell_orders.keys())
                    best_ask_volume = order_depth.sell_orders[best_ask]

                    # Check if the lowest ask (sell order) is lower than the above defined fair value
                    if best_ask < acceptable_price and cur_position - best_ask_volume <= PEARL:

                        # In case the lowest ask is lower than our fair value,
                        # This presents an opportunity for us to buy cheaply
                        # The code below therefore sends a BUY order at the price level of the ask,
                        # with the same quantity
                        # We expect this order to trade with the sell order
                        logger.print("BUY PEARL: ", str(-best_ask_volume) + "x", best_ask)
                        orders.append(Order(product, best_ask, -best_ask_volume))

                # The below code block is similar to the one above,
                # the difference is that it finds the highest bid (buy order)
                # If the price of the order is higher than the fair value
                # This is an opportunity to sell at a premium
                if len(order_depth.buy_orders) != 0:
                    best_bid = max(order_depth.buy_orders.keys())
                    best_bid_volume = order_depth.buy_orders[best_bid]
                    if best_bid > acceptable_price and cur_position - best_bid_volume >= -PEARL:
                        logger.print("SELL PEARL: ", str(best_bid_volume) + "x", best_bid)
                        orders.append(Order(product, best_bid, -best_bid_volume))

                # Add all the above orders to the result dict
                result[product] = orders

                # Return the dict of orders
                # These possibly contain buy or sell orders for PEARLS
                # Depending on the logic above
        logger.flush(state, result)
        return result